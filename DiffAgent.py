#!/usr/bin/python

#   Project:        DIFFAGENT
#   Update:         8 Aug 2016 @ 12:21 EST
#   Author:         mmiddleton
#   Version:        0.0.1


#   Import Statements: 
import sys
import os
import platform




#   menu ()
#   This function displays the menu and openning screen for DiffAgent.
def clear ():
    os.system('cls')
    os.system('clear')
    print ('=================================================')
    print ('|\t\tDIFFAGENT v0.0.1\t\t|')
    print ('=================================================')
    print ('\n')




#   sysinfo (machine) - 'host' or 'target'
#   This function displays the host machine
def info (args):
    target = args[0]

    if (target == 'host'):
        print ('\n=================================================')
        print ('System Information')
        print ('=================================================')
        print ('Operating System:\t' + os.name)
        print ('Platform:\t\t' + platform.system ())
        print ('Platform Version:\t' + platform.release())
        print ('Hostname:\t\t' + platform.node ())
        print ('=================================================')
        print ('\n')

    elif (target == 'client'):
        print ('\n=================================================')
        print ('System Information')
        print ('=================================================')
        print ('This option is in development.')
        print ('\n')

    else:
        print ('\nERROR:\tInvalid target \'' + target + '\'.')
        print ('\n')



#   help_method ()
#   This function displays the available commands for a user to input.
def help_method (args):
    topic = args[0]

    if (topic == ''):
        print ('\n=================================================')
        print ('Help Page:')
        print ('=================================================')
        print ('Possible Commands:\nclear \
        exit \
        info [target] \
        help')
        print ('\nIf you are typing a command that EXACTLY follows the proper format, there may be a bug. Please contact a developer to help with this issue.')
        print ('=================================================')
        print ('\n')

    else:
        print ('\nINFO:\tIndividual help pages coming soon...')
        print ('\n')




#   run ()
#   This function is the main run loop of the program. All interactions from the user go through here initially.
def run ():
    clear ()
    
    #=========================================
    #   MAIN RUNTIME LOOP:
    #=========================================
    while (True):
        # User input variable
        user_input = raw_input ('DIFAGENT > ')
        options = {
            'clear' : clear,
            'help' : help_method,
            'info' : info
        }

        if (user_input == 'exit'):
            break

        elif (user_input == 'clear'):
            clear ()
            continue
        
        else:
            choice = user_input.split (' ')[0]

            try:
                args = user_input.split (' ')[1:]
                options[choice] (args)

            except:
                try:
                    args.append ('')
                    options[choice] (args)

                except:
                    print 'ERROR:\tYou have entered a command that is not currently supported. To see a list of currently supported commands, please type \'help\'.'
    #=========================================
    
    print 'INFO:\tYou have exited the runtime loop. Program is now closing...'

run ()
