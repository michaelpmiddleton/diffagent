LAST UPDATE:    9 August 2016

+---------------------------------------------------+
|       DIFFAGENT: A File Difference Solution       |
+---------------------------------------------------+

1. Background Information
2. Emergency Support Contact Information
3. Update/Change Procedure
4. Code-keepers



SECTION I:  Background Information

    Servicemate was created by Michael Middleton (`18) with the purpose of assisting in
    Enterprise Applications efficiency. It was designed with the intention of having continued
    support from current Computer Science stduents OR those that are proficient in coding. All
    code will be commented so as to allow seemless transition from the present to future student
    maintaining the scripts for this project (code-keeper). This file along with all others
    relating to Servicemate are uploaded on a Bitbucket repository, of which the following
    people have been granted various levels of permission:
    
    Read/Write:
    Tim Campbell

    The link to the repository is as follows:
    https://bitbucket.org/michaelpmiddleton/diffagent/



SECTION II: Emergency Support Contact Information

    If this section is pertinent to your current situtation, it is likely that the code/script
    is broken beyond what the current code-keeper can repair. If this is the case, please contact
    the creator with the following contact points:

    Michael Middleton
    120 Pleasant St.
    Bridegwater, MA 02324-1416
    mp.middleton@outlook.com
    774-444-3853

    *Please note, as of 9 August, 2016 this is accurate contact information. It will be updated
    again after/if Michael departs for Notre Dame in Fall 2017.



SECTION III: Update/Change Procedure

    DiffAgent is maintained through use of Git project management. If you are reading this
    but are unfamiliar, please navigate with your prefered browser to http://www.git.org/ to
    get a crash-course.

    ATTENTION CODE-KEEPER:  Unless you have been authorized to update the main release version,
                            DO NOT update the MASTER branch. Create your own branch and when
                            you are certain the version will not break the product, merge your
                            branch to the master. Unless specifically told to do so, you should
                            not be making edits directly to the MASTER beyond release v1.0.


      Step 0a:  Create your own branch. This only needs to be done after you have been given
                code-keeper credentials OR if you have previously merged your branch to another
                branch. DO NOT create a new branch for every commit.

      Step 0b:  Ensure that you are editing the correct branch. If you have not done so, read the
                paragraph with the tag 'Attention Code-Keeper.'

      Step 1:     Add the pertinent files to the current commit.

      Step 2:     Commit the change with the following format:

              - BEGIN COMMIT -
              [tag] 10 word (max) summary of change

              + Sentence detailing change
              + Add as many as you need.

              - END OF COMMIT -

      The possible tags are as follows:

              major -   This tag is for when more than 200 lines of code have
                        been changed/added OR when the changes that have been
                        implemented reflect a noticeable difference from the
                        previous commit to the present commit.


                        ATTENTION CODE-KEEPER:  Please check other 'major'
                                                commits to get an idea of the
                                                proper use of the tag.


              minor -    This is the default tag. If your edit does not meet the
                         criteria of other tags, it's a safe assumption that the
                         correct tag to use is 'minor'.
              
              release -   This tag is used in conjunction with the tag command to
                          demonstrate the release of a new product version.

              Step 3:     Push the release to ORIGIN.

              Step 4:   OPTIONAL - Merge your branch to the MASTER branch. ONLY do this if your
                                   work has is a hotfix OR if you are making a release commit. 

SECTION IV: Code-Keeper list:

    ATTENTION CODE-KEEPER:  Please be sure your information is up to date. If something
                            breaks, you be contacted when current Stonehill personel
                            attempt to repair the program.


    Michael Middleton:
        Term:   2016 - 2018
        Class:  2018
        Email:  mp.middleton@outlook.com
        Cell:   774-444-3853
        First Commit:   Initial Commit  
        Last Commit:    -

    Alexander Zicaro:
        Term:   2018 - 2019
        Class:  2019
        Email:  
        Cell:   
        First Commit:   -
        Last Commit:    -
